package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int TO_MS = 1_000_000;
	private static final int START = 1_000;
	private static final int END = 2_000;
	private static final int ELEMS = 100_000;
	private static final int READS = 1_000;
	private static final long AFRICA_POPULATION = 1_110_635_000L;
	private static final long AMERICAS_POPULATION = 972_005_000L;
	private static final long ANTARCTICA_POPULATION = 0L;
	private static final long ASIA_POPULATION = 4_298_723_000L;
	private static final long EUROPE_POPULATION = 742_452_000L;
	private static final long OCEANIA_POPULATION = 38_304_000L;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> arrayList = new ArrayList<>();
    	for (int i=START; i<END; i++) {
    		arrayList.add(i);
    	}
    	System.out.println(arrayList);
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> linkedList = new LinkedList<>(arrayList);
    	System.out.println(linkedList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int first = arrayList.get(0);
    	arrayList.set(0, arrayList.get(arrayList.size()-1));
    	arrayList.set(arrayList.size()-1, first);
    	System.out.println(arrayList);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer elem : arrayList) {
    		System.out.println(elem);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for (int i=1; i<=ELEMS; i++) {
    		arrayList.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
        System.out.println("Add ELEMS = " + ELEMS
                + " as first element of the collection. Time required: " + time
                + "ns (" + time / TO_MS + "ms)");
        
        long time2 = System.nanoTime();
    	
    	for (int i=1; i<=ELEMS; i++) {
    		linkedList.add(0, i);
    	}
    	
    	time2 = System.nanoTime() - time2;
        System.out.println("Add ELEMS = " + ELEMS
                + " as first element of the collection. Time required: " + time2
                + "ns (" + time2 / TO_MS + "ms)");
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        long time3 = System.nanoTime();
    	
    	for (int i=0; i<READS; i++) {
    		arrayList.get(arrayList.size()/2);
    	}
    	
    	time3 = System.nanoTime() - time3;
        System.out.println("Reading " + READS + " times an element whose position is in the middle of the collection."
        		+ " Time required: " + time3
                + "ns (" + time3 / TO_MS + "ms)");
        
        long time4 = System.nanoTime();
    	
    	for (int i=0; i<READS; i++) {
    		linkedList.get(linkedList.size()/2);
    	}
    	
    	time4 = System.nanoTime() - time4;
        System.out.println("Reading " + READS + " times an element whose position is in the middle of the collection."
        		+ " Time required: " + time4
                + "ns (" + time4 / TO_MS + "ms)");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        Map<String,Long> world = new HashMap<>();
        world.put("Africa", AFRICA_POPULATION);
        world.put("Americas", AMERICAS_POPULATION);
        world.put("Antarctica", ANTARCTICA_POPULATION);
        world.put("Asia", ASIA_POPULATION);
        world.put("Europe", EUROPE_POPULATION);
        world.put("Oceania", OCEANIA_POPULATION);
        /*
         * 8) Compute the population of the world
         */
        long totPopulation=0;
        for (final long population : world.values()) {
        	totPopulation += population;
        }
        System.out.println("World's population: " + totPopulation);
    }
}
